﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using ShapesLib;

namespace ShapesLib.Tests
{
    public class TriangleTests
    {
        [Fact]
        public  void Calculate_Trinagle_Square()
        {
            Triangle triangle = new Triangle(12.75, 13, 16.5);

            Assert.Equal(81.53767, triangle.GetSquare());
        }

        [Fact]
        public void Pass_Invalid_Edge_Ratio_Throws_Exception() 
        {
            Assert.Throws<ArgumentException>(() => new Triangle(10, 20, 31));
        }

        [Fact]
        public void Check_If_RightTriangle()
        {
            Triangle triangle = new Triangle(5, 4, 3);

            Assert.Equal(TriangleType.Right, triangle.TriangleType);
        }

        [Fact]
        public void Pass_Negeative_Argument_Throws_Exception() 
        {
            Assert.Throws<ArgumentException>(() => new Triangle(-10.5, 12, 10));
        }
    }
}
