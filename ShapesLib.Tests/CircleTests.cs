using System;
using Xunit;
using ShapesLib;

namespace ShapesLib.Tests
{
    public class CircleTests
    {
        [Fact]
        public void Calculate_Circle_Square()
        {
            double radius = 12.524;
            Circle circle = new Circle(radius);

            Assert.Equal(Math.PI * radius * radius, circle.GetSquare());
        }

        [Fact]
        public void Calculate_Circle_Square_With_InaccuratePI() 
        {
            double radius = 10;
            Circle circle = new Circle(radius);

            Assert.NotEqual(3.14 * radius * radius, circle.GetSquare());
        }

    }
}
