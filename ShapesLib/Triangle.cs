﻿using ShapesLib.TriangleTypeRules;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;

namespace ShapesLib
{
    public class Triangle : Shape
    {
        private readonly ReadOnlyCollection<double> _edges;

        private ShapeFeature _square;

        private TriangleType _triangleType;

        public Triangle(double edgeA, double edgeB, double edgeC)
        {
            _edges = (new List<double>() { edgeA, edgeB, edgeC }).AsReadOnly();
            Validate(_edges);
        }

        /// <summary>
        /// Calculates shape of triangle
        /// </summary>
        /// <returns></returns>
        public override double GetSquare()
        {
            if (_square.IsCalculated)
                return _square.Value;

            double halfPerimeter = _edges.Sum() / 2;

            _square.Value = Math.Sqrt(halfPerimeter * (halfPerimeter - _edges[0]) * (halfPerimeter - _edges[1]) * (halfPerimeter - _edges[2]));

            return Math.Round(_square.Value, 5);
        }

        public TriangleType TriangleType
        {
            get
            {
                if (_triangleType == default)
                    _triangleType = GetTriangleType(_edges);

                return _triangleType;
            }
        }

        private void Validate(ReadOnlyCollection<double> edges) 
        {
            if (edges.Any(edge => edge < 0))
                throw new ArgumentException("Edge should be a positive value");

            if(edges[0] + edges[1] < edges[2]
                || edges[0] + edges[2] < edges[1]
                || edges[1] + edges[2] < edges[0])
                throw new ArgumentException("Invalid edge ratio");
        }

        private TriangleType GetTriangleType(ReadOnlyCollection<double> edges)
        {
            RightTriangleRule rightRule = new RightTriangleRule(edges);
            if (rightRule.IsSatisfied)
                return TriangleType.Right;

            EquilateralTriangleRule equilateralRule = new EquilateralTriangleRule(edges);
            if (equilateralRule.IsSatisfied)
                return TriangleType.Equilateral;

            return TriangleType.Basic;
        }
    }

    public enum TriangleType
    {
        None = 0,
        Basic = 1,
        Right = 2,
        Equilateral = 3
    }
}
