﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections.ObjectModel;

namespace ShapesLib.TriangleTypeRules
{
    internal class RightTriangleRule: IRule
    {
        private bool _isSatisfied;

        public RightTriangleRule(ReadOnlyCollection<double> edges)
        {
            var sortedEdges = edges.ToList();
            sortedEdges.Sort();
            
            _isSatisfied = Math.Pow(sortedEdges[2], 2) == Math.Pow(sortedEdges[1], 2) + Math.Pow(sortedEdges[0], 2);
        }

        public bool IsSatisfied => _isSatisfied;
    }
}
