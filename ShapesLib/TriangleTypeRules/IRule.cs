﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShapesLib.TriangleTypeRules
{
    internal interface IRule
    {
        bool IsSatisfied { get; }
    }
}
