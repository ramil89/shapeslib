﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ShapesLib.TriangleTypeRules
{
    internal class EquilateralTriangleRule : IRule
    {
        private bool _isSatisfied;

        public EquilateralTriangleRule(ReadOnlyCollection<double> edges)
        {
            _isSatisfied = edges[0] == edges[1] && edges[1] == edges[2];
        }

        public bool IsSatisfied => _isSatisfied;
    }
}
