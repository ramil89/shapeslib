﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShapesLib
{
    public class Circle: Shape
    {
        private ShapeFeature _square;

        private double _radius;

        public Circle(double radius)
        {
            if(radius < 0)
                throw new ArgumentException("Radius should be positive value.");

            _radius = radius;
        }

        public override double GetSquare()
        {
            if (_square.IsCalculated)
                return _square.Value;

            _square.Value = Math.PI * _radius * _radius;

            return _square.Value;
        }
    }
}
