﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShapesLib
{
    public abstract class Shape
    {
        public abstract double GetSquare();
    }
}
