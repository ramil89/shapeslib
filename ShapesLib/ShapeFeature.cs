﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShapesLib
{
    internal struct ShapeFeature
    {
        private double _square;

        public double Value
        {
            get
            {
                return _square;
            }
            set
            {
                _square = value;
                IsCalculated = true;
            }
        }

        public bool IsCalculated { get; private set; }
    }
}
